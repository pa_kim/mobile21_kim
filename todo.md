Module tasklist
Complete the tasks below and remember to reflect your work in the learning diary.
Follow the steps and complete the coding-tasks in each video.

Introduction [Part 1].
https://www.youtube.com/watch?v=dFlPARW5IX8
1. In this video you will learn how to set up an Android Studio Project.
2. How Android Studio can be used to make a simple app.
3. How to debug and run an app in Android Studio

Core Elements [Part 2].
https://www.youtube.com/watch?v=6ow3L39Wxmg
1. In this video you will learn about core elements to android development.
2. You will learn what are Activities, Intents, IntentServices and BroadcastReceivers

Lists, Layouts, and Images [Part 3].
https://www.youtube.com/watch?v=rdGpT1pIJlw
1. In this video you will learn how to use ListView.
2. How to create a custom layout component.
3. How to incorporate images with Image View


Project task
Your project should have the following:

1. Functionality with components (ex. buttons, text fields, togglers etc.)
2. Multiple views
3. A component to display information (ex. listView)

You are free to add anything you think of.

Make your project look more like you. It can be ex. styles or some other logic.

You can add ex. some of these if you want:

Android Guides [Android Guides].

Make your project represent your skills, so that you may use this project as a solid proof of your skills when applying for a developer position.