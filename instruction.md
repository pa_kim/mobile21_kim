The course has 3 mandatory assignments for a student to pass this course.  
 1. Material from the exercise projects that you perform, following the tutorial series (Reflect your learning in the diary) 
 2. Learning Diary to keep track and reflect your learning. (Template found at the bottom of this page)
 3. Project to demonstrate how to use the course material in action.

How to actually complete the course?  
1. Get yourself familiar with Git and choose a code-editor
2. Follow the steps provided in this Moodle course        
3. Remember to commit your work often, so it's easier to keep track of your work.
4. It's recommended to commit your coursework in a different directory in your git-repository apart from your project, label the directory ex. Coursework
5. Remember to write your learning diary side by side with every time you work with course material, this is very highly recommended!
6. Return your work to Moodle.
7. Go to the "Course completion" tab and select that you want your course to be graded.


You will submit a file to moodle containing a link to your public git repository.

In your git repository there should be the following:
1. Material from your exercise projects
2. Learning Diary
3. Your Project (source code and running / compiling instructions in the readme)
4. ReadME how to run your project
5. File including a link to a video of your project running 

